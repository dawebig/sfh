var webpack = require('webpack');

module.exports = {
    entry: [
        'babel-polyfill',
        './app/src/index.js'
    ],
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',
            query:
            {
                presets:['es2015','react']
            }
        }]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    output: {
        path: './app/build',
        publicPath: '/',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: './app/build',
        historyApiFallback: true,
        hot: true,
        inline: true,
        progress: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};